package item1;


public class Company implements Taxable {
	String name ;
	double income;
	double expense;
	double profit;
	
	public Company (String name , double income,  double expense){
		this.name = name ;
		this.income = income;
		this.expense = expense;
		this.profit = income-expense;
	}

	public double getIncome(){
		return income;
	}
	
	public double getExpense(){
		return expense;
	}
	
	public double getProfit(){
		return profit;
	}

	@Override
	public double getTax() {
		int tax = 0;
		tax = (int) (profit*0.3);
		return tax;
	}
	
	public String toString(){
		return name +" Income: "+ income +" Expenses: "+ expense +" Profit: "+ profit;
	}
}

