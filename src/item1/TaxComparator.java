package item1;
import java.util.Comparator;


public class TaxComparator implements Comparator<Taxable>{
	@Override
	public int compare(Taxable c1, Taxable c2) {
		if(c1.getTax() <  c2.getTax()) return -1;
	    if(c1.getTax() == c2.getTax()) return 0;
	       return 1;
	}	
}
