package item2;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal {

	ArrayList<Node> n ;
	
	public PreOrderTraversal(){
		n = new ArrayList<Node>();
	}
	@Override
	public ArrayList<Node> traverse(Node node) {
		if (node!=null){
			n.add(node);
			traverse(node.getLeft());
			traverse(node.getRight());
			
		}
		return n;
	}

}
