package item2;

public class ReportConsole {
	
	public void display(Node root,Traversal traverse){
		for (Node n : traverse.traverse(root)) {
			System.out.print(n.getValue()+" ");
		}
		System.out.println("");
	}
}
