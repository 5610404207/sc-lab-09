package item1;

public interface Taxable {

	public double getTax();
}