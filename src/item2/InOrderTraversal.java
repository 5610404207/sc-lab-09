package item2;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal {
	ArrayList<Node> n ;
	
	public InOrderTraversal(){
		n = new ArrayList<Node>();
	}
	@Override
	public ArrayList<Node> traverse(Node node) {
		if (node!=null){
			traverse(node.getLeft());
			n.add(node);
			traverse(node.getRight());
		}
		return n;
	}

}