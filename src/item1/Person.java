package item1;


public class Person implements Taxable,Comparable<Person> {
	String name ;
	double salary;
	
	public Person (String name , double salary){
		this.name = name ;
		this.salary = salary;
	}

	@Override
	public int compareTo(Person other) {
		if (this.salary < other.salary ) { return -1; }
		if (this.salary > other.salary ) { return 1;  }
		return 0;
	}

	@Override
	public double getTax() {
		double tax = 0;
		if (salary > 300000){
			tax = (salary-300000)*0.1+15000;
		}
		else{
			tax = salary*0.05;
		}
		
		return tax;
	}

	public String toString(){
		return name +" "+ salary;
	}
}
