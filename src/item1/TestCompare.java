package item1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;




public class TestCompare {
	public static void main(String[] args) {
		ArrayList<Person> psl = new ArrayList<Person>();
		psl.add(new Person("kittipat",180.00));
		psl.add(new Person("jirapinya",156.00));
		
		System.out.println("---- Before sort");
		for (Person p : psl) {
			System.out.println(p);
		}
		
		Collections.sort(psl);
		
		System.out.println("\n---- After sort");
		for (Person p : psl) {
			System.out.println(p);
		}
		
		ArrayList<Product> pdl = new ArrayList<Product>();
		pdl.add(new Product("Disney",200000.00));
		pdl.add(new Product("Hell",100000.00));
		
		System.out.println("\n---- Before sort");
		for (Product p : pdl) {
			System.out.println(p);
		}
		
		Collections.sort(pdl);
		
		System.out.println("\n---- After sort");
		for (Product p : pdl) {
			System.out.println(p);
		}
		
		ArrayList<Company> cpl = new ArrayList<Company>();
		cpl.add(new Company("kittipat",500.00,180.00));
		cpl.add(new Company("jirapinya",200.00,156.00));
		
		System.out.println("\n---- Before sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		Collections.sort(cpl,new EarningComparator());
		
		System.out.println("\n---- After sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		System.out.println("\n---- Before sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		Collections.sort(cpl,new ExpenseComparator());
		
		System.out.println("\n---- After sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		System.out.println("\n---- Before sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		Collections.sort(cpl,new ProfitComparator());
		
		System.out.println("\n---- After sort");
		for (Company p : cpl) {
			System.out.println(p);
		}
		
		ArrayList<Taxable> tl = new ArrayList<Taxable>();
		tl.add(new Person("jirapinya",156.00));
		tl.add(new Product("Disney",200000.00));
		tl.add(new Company("kittipat",500.00,180.00));
		
		System.out.println("\n---- Before sort");
		for (Taxable t : tl) {
			System.out.println(t);
		}
		
		Collections.sort(tl,new TaxComparator());
		
		System.out.println("\n---- After sort");
		for (Taxable t : tl) {
			System.out.println(t);
		}
	}
}
