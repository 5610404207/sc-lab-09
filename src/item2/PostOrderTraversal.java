package item2;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal {
	ArrayList<Node> n ;
	
	public PostOrderTraversal(){
		n = new ArrayList<Node>();
	}
	
	@Override
	public ArrayList<Node> traverse(Node node) {
		if (node!=null){
			
			traverse(node.getLeft());
			traverse(node.getRight());
			n.add(node);
		}
		return n;
	}

}