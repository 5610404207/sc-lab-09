package item2;

import java.util.ArrayList;

public interface Traversal {

	public ArrayList<Node> traverse(Node node);
}
